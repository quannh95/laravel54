<?php

namespace App;
use Carbon\Carbon;

class Post extends Model
{
    public function comments()

    {
    	return $this->hasMany(Comment::class);
    }

    public function addComment($user_id, $body)

    {

    	$this->comments()->create(compact('user_id','body'));

    }
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    public function scopeFillter($query, $fillers)
    {
        if($month = $fillers['month']){
            $query->whereMonth('created_at', Carbon::parse($month)->month);
        }
        if($year = $fillers['year']){
            $query->whereYear('created_at', $year);
        }

    }

    public static function archives()
    {
        return static::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
            ->groupby('year', 'month')
            ->orderByRaw('min(created_at) desc')
            ->get()
            ->toArray();
    }
}
