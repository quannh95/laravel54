<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request,Hash;

class RegistrationController extends Controller
{
    public function create()
    {
    	return view('examples.registration.create');
    }
    public function store()
    {
    	// Validate the form
    	$this->validate(request(),[
    		'name' => 'required|max:100|min:3',
    		'email' => 'required|email|unique:users,email|max:100',
    		'password' => 'required|confirmed'
    	],[
    		'name.required' => 'The name field is required',
    		'name.min' => 'The name must be at least 3 characters!',
    		'name.max' => 'The name may not be greater than 100 characters!',
    		'email.required' => 'The email field is required',
    		'email.unique' => 'The email was existed!',
    		'email.max' => 'The name may not be greater than 100 characters!',
    		'email.email' => 'Not the correct email format!',
    		'password.required' => 'The password field is required',
    		'password.confirmed' => 'The password confirm does not match'
    	]);
    	$user = User::create([
    		'name' => request('name'),
    		'email' => request('email'),
    		'password' => Hash::make(request('password'))
    	]);
        session()->flash('message', 'Thanks so much for signing up.');

    	auth()->login($user);

    	return redirect()->home();
    }
}
