<?php

namespace App\Http\Controllers;
use App\Post;

use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);

    }

    public function index()

    {
    	$posts = Post::latest()
            ->fillter(request(['month', 'year']))
            ->get();
        return view('examples.posts.index', compact('posts'));
    }

    public function show(Post $post)

    {
    	return view('examples.posts.show', compact('post'));
    }

    public function create()

    {

    	return view('examples.posts.create');

    }

    public function store(Request $request)

    {
        $this->validate(request(),[
            'title' => 'required|max:30',
            'body' =>'required|min:5'
        ],[
            'title.required' => 'The field title required!',
            'title.max' => 'The title may not be greater than 30 characters!',
            'body.required' => 'The field body required!',
            'body.min' =>'Body must be at least 5 characters!'
        ]);

        auth()->user()->publish(
            new Post(request(['title', 'body']))
        );

        return redirect('/blog');
    }
}
