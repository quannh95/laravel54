<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;
use Auth;

class CommentsController extends Controller
{
    public function store($postid)
    {
        $post = Post::find($postid);
        
        if(Auth::check()) {
            $user_id = Auth::user()->id;
            $this->validate(request(),[
            'body' => 'min:1'
            ],[
                'body.min' => 'The field is not null'
            ]);

            $post->addComment($user_id, request('body'));
        }
    	

    	return back();
    }
}
