@extends('examples.partials.master')

@section('content')
	<div class="col-sm-8 blog-main">
		<h1>{{ $post->title }}</h1>

		{{ $post->body }}

		<hr>
		
		<ul class="list-group">
		<div class="comments">
		@foreach($post->comments as $comment)
			<li class="list-group-item">
				<strong>
					{{$comment->user->name}}&nbsp;-&nbsp;{{ $comment->created_at->format('m/d/Y H:i:s') }}: &nbsp;
				</strong>
				{{ $comment->body }}
			</li>
		@endforeach
		</div>
		</ul>
		<hr>
		<div class="card">
			<div class="cart-block">
				<form method="POST" action="/posts/{{ $post->id }}/comments">

					{{ csrf_field() }}

					<div class="form-group">
						<textarea name="body" placeholder="Your comment here." class="form-control"></textarea>
						<span class="text text-danger">{{ $errors->first('body') }}</span>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Add comment</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection