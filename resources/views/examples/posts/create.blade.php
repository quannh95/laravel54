@extends('examples.partials.master')

@section('content')
	<div class="col-sm-8 blog-main">
		<h1>Create a post</h1>
		
		<hr>

		<form method="POST" action="/posts">
		  {{ csrf_field() }}
		  <div class="form-group" {{ $errors->has('title') ? 'has-error' : '' }}>
		    <label for="title">Title:</label>
		    <input type="text" class="form-control" name="title">
		    <span class="text-danger">{{ $errors->first('title') }}</span>
		  </div>

		  <div class="form-group" {{ $errors->has('body') ? 'has-error' : '' }}>
		    <label for="body">Body</label>
		    <textarea id="body" name="body" class="form-control"></textarea>
		    <span class="text-danger">{{ $errors->first('body') }}</span>
		  </div>
		<button type="submit" class="btn btn-primary">Publish</button>

		</form>
	</div>
@endsection