@extends('examples.partials.master')
@section('content')
	<div class="col-sm-8">
		<h1>Register</h1>
		<form method="POST" action="/blog/register">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" class="form-control" id="name" name="name" required>
				<span class="text text-danger">{{ $errors->first('name') }}</span>
			</div>	
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" class="form-control" id="email" name="email" required>
				<span class="text text-danger">{{ $errors->first('email') }}</span>
			</div>
			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" class="form-control" id="password" name="password" required>
				<span class="text text-danger">{{ $errors->first('password') }}</span>
			</div>	
			<div class="form-group">
				<label for="password">Password Confirmation:</label>
				<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
				<span class="text text-danger">{{ $errors->first('password') }}</span>
			</div>	
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Register</button>
			</div>
		</form>
	</div>
@endsection