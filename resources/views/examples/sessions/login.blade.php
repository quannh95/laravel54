@extends('examples.partials.master')
@section('content')
	<div class="col-sm-8">
		<h1>Log In</h1>
		<form method="POST" action="/blog/login">
			{{ csrf_field() }}
			@if(count($errors) > 0)
				@foreach($errors->all() as $error)
					<div class="alert alert-danger">{{ $error }}</div>
				@endforeach
			@endif
			<div class="form-group">
				<label for="email">Email Address:</label>
				<input type="email" class="form-control" id="email" name="email" required>
				<span class="text text-danger">{{ $errors->first('email') }}</span>
			</div>
			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" class="form-control" id="password" name="password" required>
				<span class="text text-danger">{{ $errors->first('password') }}</span>
			</div>		
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Log In</button>
			</div>
		</form>
	</div>
@endsection