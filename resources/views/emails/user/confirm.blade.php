 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Registration confirmation</title>
 	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
 	<style type="text/css">
 		a{
 			text-decoration: none;
 		}
		.btn-primary {
		    color: #fff;
		    background-color: #337ab7;
		    border-color: #2e6da4;
		}
		.btn {
		    display: inline-block;
		    padding: 6px 12px;
		    margin-bottom: 0;
		    font-size: 14px;
		    font-weight: 400;
		    line-height: 1.42857143;
		    text-align: center;
		    white-space: nowrap;
		    vertical-align: middle;
		    -ms-touch-action: manipulation;
		    touch-action: manipulation;
		    cursor: pointer;
		    -webkit-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		    background-image: none;
		    border: 1px solid transparent;
		    border-radius: 4px;
		}
	</style>
 </head>
 <body>
 	Hi, {{ $name }} <br/>
	Welcome to my App <br/>

	Thanks for signing up. Please click below button to active your account!<br>
		<a href="{{ route('confirmation', $token) }}" style"text-underline">
		<button class="btn btn-primary">
			Activation
		</button>
		</a>
		<br>
		
	Thanks,<br>
	{{ config('app.name') }}
 </body>
 </html>

